CC=clang --std=gnu17 -ferror-limit=3 -O3

.PHONY: run clean all install

libpns.so: pns.h pns.c
	$(CC) -fPIC -shared pns.c -o libpns.so -Wall -Werror

all: pns.o libpns.so test

install: libpns.so
	cp pns.h /usr/local/include/
	cp libpns.so /usr/local/lib/

run: test
	./test

test: testing/test.c pns.o
	$(CC) testing/test.c -o test pns.o -Wall -Werror

pns.o: pns.h pns.c
	$(CC) -c pns.c -o pns.o -Wall -Werror

clean:
	rm -f test pns.o libpns.so examples/example.pnsl

