#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>

#include "../pns.h"

void localTests() {
    PnsNet net;
    pnsCreateNet(&net);
    size_t pid_tmp = pnsNet_addPlace(&net);
    pnsNet_removePlace(&net, pid_tmp);
    printf("Current place count: %zu\n", net.places.count);
    size_t pid1 = pnsNet_addPlace(&net);
    size_t pid2 = pnsNet_addPlace(&net);
    size_t tid = pnsNet_addConnectedTransition(&net, 1, &pid1, 0, NULL);
    pnsNet_connectTransitionToPlace(&net, tid, pid2);
    pnsNet_removePlace(&net, pid1);
    pnsNet_removePlace(&net, pid2);
    pnsDestroyNet(&net);
}

int main(int argc, char** argv) {
    localTests();
    printf("\n");

    PnsNet net;

    struct stat st; 

    if (stat("examples/example.pn", &st) != 0) {
        fprintf(stderr, "Cannot determine size of net file\n");
        exit(EXIT_FAILURE);
    }

    size_t size = (st.st_size + 3) / 4;
    uint32_t values[size];

    FILE* loadFile = fopen("examples/example.pn", "rb");
    fread(values, sizeof(uint32_t), size, loadFile);
    
    fclose(loadFile);

    if (!pnsLoadNet(&net, size, values)) {
        fprintf(stderr, "Failed loading of net\n");
        exit(EXIT_FAILURE);
    }

    if (argc >= 2 && !strncmp(argv[1], "net", 3)) {
        FILE* saveFile = fopen("examples/example.pn", "wb");

        if (saveFile) {
            size_t count = pnsNet_serializeSize(&net);
            uint32_t elements[count];
            pnsNet_serialize(&net, elements);

            fwrite(elements, sizeof(size_t), count, saveFile);

            fclose(saveFile);

            printf("Saving successful\n");
            exit(EXIT_SUCCESS);
        }
        else {
            printf("Saving failed\n");
            exit(EXIT_FAILURE);
        }
    }
    PnsNet net_clone;
    pnsCloneNet(&net_clone, &net);
    pnsDestroyNet(&net);
    net = net_clone;
    PnsState state;
    if (argc >= 2 && !strncmp(argv[1], "load", 4)) {
        struct stat st;

        if (stat("examples/example.pns", &st) != 0) {
            fprintf(stderr, "Cannot determine size of state file\n");
            exit(EXIT_FAILURE);
        }

        size_t size = (st.st_size + 3) / 4;
        uint32_t values[size];

        FILE *loadFile = fopen("examples/example.pns", "rb");
        if (!loadFile) {
            fprintf(stderr, "Opening state file for loading failed\n");
            exit(EXIT_FAILURE);
        }

        fread(values, sizeof(uint32_t), size, loadFile);

        bool result = pnsLoadState(&state, &net, size, values);
        if (!result) {
            fprintf(stderr, "State initialization failed\n");
            exit(EXIT_FAILURE);
        }
    }
    else {
        pnsCreateState(&state, &net);
    }
    PnsState state_clone;
    pnsCloneState(&state_clone, &state, &net);
    pnsDestroyState(&state);
    state = state_clone;

    size_t MAX_COUNT = 64;
    char names[MAX_COUNT * net.transitions.count];

    FILE *file = fopen("examples/example.pnk","r");
    for (size_t i = 0; i < net.transitions.count; ++i) {
        char* name = &names[i * MAX_COUNT];
        getline(&name, &MAX_COUNT, file);
    }
    fclose(file);

    bool forward = true;

loop: {
        PnsTransitionView (*state_transitions) (const PnsState* state);
        void (*state_fire) (PnsState* state, const PnsNet* net, size_t tid);
        if (forward) {
            state_transitions = pnsState_transitions;
            state_fire = pnsState_fire;
        }
        else {
            state_transitions = pnsState_transitions_backward;
            state_fire = pnsState_fire_backward;
        }
        PnsTransitionView view = state_transitions(&state);
        size_t count = view.count;
        if (count == 0) goto reverse;
        size_t transitions[count];
        for (int i = 0; i < count; ++i) pnsTransitionView_next(&view, &transitions[i]);
        printf("Choose a transition:\n");
        for(size_t i = 0; i < count; ++i) {
            printf("%lu: %s", i + 1, &names[transitions[i] * MAX_COUNT]);
        }
        char num[8];
        char* n = num;
        size_t bytes = 7;
        printf("> ");
        getline(&n, &bytes, stdin);
        printf("\n");
        if (*n == 10) goto loop;
        switch (*n) {
        case 'r':
            goto reverse;
        case 'q':
            goto end;
        case 's':
            goto save;
        }
        size_t select = atoi(num);
        if (select != 0 && select <= count) {
            state_fire(&state, &net, transitions[select - 1]);
        }
        else {
            printf("You have to input a valid number from 1 to %zu\n", count);
            printf("\n");
            printf("Other options:\n");
            printf("q: Quit\n");
            printf("r: Reverse play direction\n");
            printf("s: Save\n");
            printf("\n");
        }
    }
    goto loop;

reverse:
    forward = !forward;
    printf("Reverse play direction!\n");
    printf("\n");
    goto loop;

end:
    pnsDestroyState(&state);
    pnsDestroyNet(&net);
    return EXIT_SUCCESS;

save: {}
    FILE *saveFile = fopen("examples/example.pns", "wb");
    if (!saveFile) {
        fprintf(stderr, "Opening state file for saving\n");
        exit(EXIT_FAILURE);
    }

    uint32_t call_counts[net.transitions.count];
    for (size_t i = 0; i < net.transitions.count; ++i) {
        call_counts[i] = state.calls.counts[i];
    }

    if (net.transitions.count == fwrite(call_counts, sizeof(uint32_t), net.transitions.count, saveFile)) {
        printf("Saving state successful\n");
    }
    else {
        printf("Saving state failed\n");
    }
    printf("\n");

    fclose(file);


    goto loop;
}

